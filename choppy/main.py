#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""CHOPPY PROGRAM

author: Alix Chagué
date: 21/08/2020

TODO @ Alix: increase layout complexity
"""

import argparse
#import os
#import sys

#from bs4 import BeautifulSoup
import pandas
#import tqdm

from utils import utils
from test import test


def identify_schema(xml_tree):
    """Identify the standard used in an XML tree and returns both standard and version

    :param xml_tree: BeautifulSoup object
    :return dict: standard and version (default values: None)
    """
    schema = {'standard': None, 'version': None}
    # None means unknown
    if xml_tree.contents[0].name == 'PcGts':
        utils.report("Looks like it's an XML PAGE file", "H")
        schema['standard'] = 'Page'
        # TODO @ Alix: identify version
    elif xml_tree.contents[0].name == 'alto':
        utils.report("Looks like it's an XML ALTO file", "H")
        schema['standard'] = 'Alto'
        # TODO @ Alix: identify version
    else:
        utils.report(f"I don't know how to interpret this root: '{xml_tree.contents[0].name}'", "E")
    return schema


def get_line_coords(xml_tree, schema):
    """Get a list of line coordinates in an xml document

    :param xml_tree: parsed XML tree
    :param schema dict: information on the schema used in the xml tree
    :return list: list of baselines (tuples: xa, xb)
    """
    # whatever the initial schema, in the output of this function the data is always organized the same way
    # model: line = {"starts_at": 0, "stops_at": 0}
    # assuming the image is vertical, for now we focus on x values and not y values
    # also that is assuming the image is not tilted, which is something we will get to later.
    lines = []
    if schema['standard'] == 'Page':
        for bl in xml_tree.find_all('Baseline'):
            # PcGts//Page/TextRegion/TextLine/Baseline/@points
            # model: "xa,ya xb,yb"
            xstarts_at = False
            xstops_at = False
            points = bl.attrs['points'].split()
            if len(points) > 1:
                # we'll ignore baseline defined by only one coordinate but won't raise any warning
                xstarts_at = points[0].split(',')[0]
                xstops_at = points[-1].split(',')[0]
            if xstops_at and xstarts_at:  # I know this if statement is unnecessary...
                lines.append((int(xstarts_at), int(xstops_at)))
    elif schema['standard'] == 'Alto':
        for bl in xml_tree.find_all('TextLine'):
            # alto//Layout/PrintSpace/TextBlock/TextLine/@BASELINE
            # model: "xa ya xb yb"
            xstarts_at = False
            xstops_at = False
            points = bl.attrs['BASELINE']
            if len(points.split()) > 4:
                utils.report("There are more than 4 points in the ALTO file. Ex: {points}", "W")
            else:
                xstarts_at, ystarts_at, xstops_at, ystops_at = points.split()
            if xstops_at and xstarts_at:  # I know this if statement is unnecessary...
                lines.append((int(xstarts_at), int(xstops_at)))
    return lines


def group_baselines(lines):
    """Build groups of lines depending on their extreme coordinates

    :param lines list: list of tuples: [(xa, xb), (xa', xb'), ...]
    :return bool|dict: False if this step failed otherwise dict containing sliced list of lines and gap's min/max
    """
    """
    At this point, the function 'fails' if:
     - there are less or more than 2 columns of text (which might be because the layout is too complexe or because the 
     space between the column is too narrow in which case Q3 is too large.
     - the two columns somehow overlap (ex: if the image is tilted)
    """
    # sorting the lines based on xa values
    lines = sorted(lines, key=lambda line: line[0])
    # distribution: [(e, i)...] where e = gap, i = index
    distribution = []
    for line in lines:
        i = lines.index(line)
        if i == 0:
            dis = (0, i)
        else:
            dis = (abs(lines[i - 1][0] - line[0]), i)
        distribution.append(dis)
    stats = pandas.DataFrame(distribution)[0].describe()
    # Quartile 3 is used to spot the biggest gap(s) | print("Q3:", stats['75%'])
    gaps = [value for value in distribution if value[0] > stats['75%']]
    # Reversing the list of indexes to slice the list of lines more easily
    gaps = sorted(gaps, key=lambda gap: gap[1], reverse=True)
    if len(gaps) == 0:
        groups = lines
    elif len(gaps) == 1:
        groups = [lines[:gaps[0][1]], lines[gaps[0][1]:]]
    else:
        remainder = lines[:]
        groups = []
        for gap in gaps:
            sub = remainder[gap[1]:]
            remainder = remainder[:gap[1]]
            groups.append(sub)
        groups.append(remainder)
    utils.report(f"I found {len(groups)} group(s) of lines in this document", "H")
    # re-establishing ascendant order between groups
    groups = sorted(groups, key=lambda group: group[0][0])
    # verify hypothesis -> at this point we return False if the layout is too complex
    # TODO @ Alix: handle exceptions...
    if len(groups) != 2:
        utils.report("I found too many or not enough groups of lines, let's stop here", "E")
        return False
    else:
        ys_g1 = [val[1] for val in groups[0]]
        xs_g2 = [val[0] for val in groups[1]]
        biggest_y = sorted(ys_g1)[0]
        smallest_x = sorted(xs_g2)[-1]
        if biggest_y >= smallest_x:
            utils.report("The 2 identified groups overlap, let's stop here", "E")
            return False
        else:
            return {'groups': groups, 'limits': {'min': biggest_y, 'max': smallest_x}}


def tell_me_where_to_chop(limits):
    """Return a coordinate between a min and a max value.

    :param limits dict: min and max values
    :return int: x coordinate
    """
    # let's cut in the middle, right?
    return limits['max'] - ((limits['max'] - limits['min']) // 2)


def main():
    utils.report("running main()", 'H')
    #/media/sf_host_achague/host_achague/Documents/DH-projects/choppy/data/page_dummy.xml
    #../data/alto_dummy.xml
    tree = utils.read_file('../data/alto_dummy.xml', 'xml')
    baselines = get_line_coords(tree, identify_schema(tree))
    groups_of_baselines = group_baselines(baselines)
    if groups_of_baselines:
        where_to_chop = tell_me_where_to_chop(groups_of_baselines['limits'])
        utils.report(f"You should cut the image on coord {where_to_chop} of x axis.", "S")
        # TODO @ Alix : generate overview of the cut image.
    else:
        utils.report("Exiting program.", "E")
    # TODO @ Alix: load the image
    # TODO @ Alix: cut the image
    # TODO @ Alix: generate a report (with Panda?)
    # TODO @ Alix: make it iterable


# ============================================================================================================
parser = argparse.ArgumentParser(description="How much wood would a wood chop chop in a wood chop could chop wood?")
parser.add_argument('-m', '--mode', action='store', nargs=1, default='default', help="default|test")
# parser.add_argument('-m', '--mode', action='store', nargs=1, default='test', help="default|test")
args = parser.parse_args()

if vars(args)['mode'].lower() == 'test':
    test.gentest()
elif vars(args)['mode'].lower() == 'default':
    main()
else:
    utils.report(f"{vars(args)['mode']} is not a valid mode", "E")
