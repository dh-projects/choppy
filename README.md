# Choppy
> A smart Python program to chop your images. 

Choppy will detect the best way to split the image of a double-paged digitization. It might even do it for you!

---

![example](static/img/example.png)