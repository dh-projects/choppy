#!/bin/sh

# requirements sur slave
#(sudo) apt-install python-pip
#(sudo) apt install virtualenv

echo "### ----- BUILDING ENVIRONMENT ----- ###"
cd ..
FILE = $PWD/venv  # looking for '..../choppy/venv'
if ! -d "$FILE" ; then
  echo "creating virtual environment 'venv'"
  virtualenv venv -p python3
fi

. venv/bin/activate  # "source venv/bin/activate" fails
python --version
echo ">> getting requirements"
pip install -r requirements.txt

echo "### ----- LINTER ----- ###"
for entry in choppy/*.py
do
    pylint-fail-under --fail_under 7.0 $entry
    # adjust grade if necessary
    # will use .pylintrc in master
done



#echo "### ----- TEST ----- ###"
# test.py for additional functional tests
#python test.py
