Run pylint during development using `.wip.pylintrc`: 

``` bash
for entry in choppy/*.py ; do pylint-fail-under --fail_under 7.0 $entry --rcfile .wip.pylintrc ; done
```

OR

``` bash
pylint-fail-under --fail_under 7.0 choppy/main.py --rcfile .wip.pylintrc
```